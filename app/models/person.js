var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PersonSchema = new Schema({
    name : String,
    surname: String,
    age: Number,
    mother: String,
    father: String
});

module.exports = mongoose.model('Person', PersonSchema);