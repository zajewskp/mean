var Person = require('./models/person');
var express = require('express');
var validator = require('express-validator');

    module.exports = function(app) {

        var router = express.Router();
        router.use(validator());
        router.use(function(req, res, next) {
            console.log('Something is happening.');
            next();
        });

        router.get('/', function(req, res) {
            res.json({ message: 'hooray! welcome to our api!' });   
        });
        router.route('/people')

            .post(function(req, res) {    
                req.checkBody('name', "Name must not be empty").notEmpty();
                req.checkBody('surname', "Surname must not be empty").notEmpty();
                req.checkBody('age', 'Invalid age').notEmpty().isInt({min: 0, max: 130});
                req.checkBody('mother', "Mother's name must not be empty").notEmpty();
                req.checkBody('father', "Father's name must not be empty").notEmpty();
                var errors = req.validationErrors();
                if(errors){
                    var messages = [];
                    errors.forEach(function(error) {
                        messages.push(error.msg);
                    });
                    res.json({ message: messages});
                    return;
                }
                var person = new Person();      
                person.name = req.body.name;
                person.surname = req.body.surname;
                person.age = parseInt(req.body.age);
                person.mother = req.body.mother;
                person.father = req.body.father;

                person.save(function(err) {
                    if (err)
                        res.send(err);

                    res.json({ message: 'Person created!' });
                });      
            })
            .get(function(req, res) {
                var limit = req.query.limit;
                Person.find()
                .sort({'surname': -1})
                .limit(limit)
                .exec(function(err, people) {
                    if (err)
                        res.send(err);

                    res.json(people);
                });
            });

        router.route('/people/:person_id')

            
            .get(function(req, res) {
                Person.findById(req.params.person_id, function(err, person) {
                    if (err)
                        res.send(err);
                    res.json(person);
                });
            })

            .put(function(req, res) {
                Person.findById(req.params.person_id, function(err, person) {
                    if (err)
                        res.send(err);

                    person.name = req.body.name; 
                    person.name = req.body.name;
                    person.surname = req.body.surname;
                    person.age = parseInt(req.body.age);
                    person.mother = req.body.mother;
                    person.father = req.body.father;
                    
                    person.save(function(err, person) {
                        if (err)
                            res.send(err);

                        res.json({ message: 'Person updated!', _id: person.id });
                    });

                });
            })

            .delete(function(req, res) {
                Person.remove({
                    _id: req.params.person_id
                }, function(err, person) {
                    if (err)
                        res.send(err);

                    res.json({ message: 'Successfully deleted' });
                });
            });
        app.use('/api', router);

        app.get('*', function(req, res) {
            res.sendfile('./public/index.html');
        });

    };