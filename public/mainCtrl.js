angular.module('sampleApp')
	.constant("categoryActiveClass", "btn-primary")
	.constant("peopleListPageCount", 10)
	.constant("dataUrl", "http://localhost:8080/api/people")
	.controller('MainCtrl',['$scope', '$filter', '$http', 'categoryActiveClass', 'peopleListPageCount', 'dataUrl', 'PeopleService', function($scope, $filter, $http, categoryActiveClass, peopleListPageCount, dataUrl, PeopleService) {
		$scope.selectedPage = 1;
		$scope.indexFrom = 0;
		$scope.pageSize = peopleListPageCount;


        $scope.data = {people:[]};



    	var selectedCategory = null;

    	$scope.selectCategory = function (newCategory){
    		var parameter = '';
    		if(newCategory === "short"){
    			var parameter = '?limit=10'
    		}
    		PeopleService.get(parameter).then(function (response){
	   			$scope.data.people = response.data;
	   			$scope.data.message = response.data.message;
		   },function (error){
		   		$scope.data.error = error;
		   });

			$scope.selectedPage = 1;
    		selectedCategory = newCategory;

    	}
    	$scope.getPageClass = function(page){
    		return $scope.selectedPage == page ? categoryActiveClass : "";
    	}
    	$scope.selectPage = function (newPage) {


    		$scope.selectedPage = newPage;
    		$scope.indexFrom = ($scope.selectedPage - 1) * $scope.pageSize;

    	}

    	$scope.add = function(personData){
    		if($scope.form.$valid){
	    		PeopleService.create(personData).then(function (response){
			   		personData._id = response.data._id;
		   			$scope.data.people.push(personData);
		   			$scope.reset($scope.form);
			   },function (error){
			   		$scope.data.error = error;
			   });
			}
    	}

    	$scope.reset = function(form){
    		if (form) {
		      form.$setPristine();
		      form.$setUntouched();
		    }
		    $scope.newPerson = {};
    	}

    	$scope.delete = function(id, index){
    		PeopleService.delete(id).then(function (response){	
				$scope.data.people.splice(index, 1);
		   },function (error){
		   		$scope.data.error = error;
		   });
    	}
    	$scope.reset();
    	$scope.selectCategory('short');
	    $scope.$watch(
  		function() { return $scope.data.people.length; },
  		function(newValue, oldValue) {
		    if ( newValue !== oldValue ) {
		  	    $scope.numberPages = Math.ceil($scope.data.people.length / peopleListPageCount);
			    $scope.counter = Array;
		    }
  		});

	}]);