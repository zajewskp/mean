angular.module('sampleApp').factory('PeopleService', ['$http', function($http) {

    return {
        get : function(limit) {
            return $http.get('/api/people?limit=' + limit);
        },

        create : function(personData) {
            return $http.post('/api/people', personData);
        },

        delete : function(id) {
            return $http.delete('/api/people/' + id);
        }
    }       

}]);